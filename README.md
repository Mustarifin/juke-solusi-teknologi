### Installing

A step by step series of examples that tell you how to get a development env running

1.  `$ git clone <your repo>`
2.  `$ composer install`
3. `$ preparing database on local`
4.  Create **.env** file as per **.env.example**. #REQUIRED line must be change
5.  `$ php artisan key:generate`
6.  `$ php artisan storage:link`
7.  `$ php artisan migrate --seed` OR `$ php artisan migrate:refresh --seed`
8.  `$ npm install`
9.  `$ php artisan serve`
10.  Access from Browser `$ localhost:8000`