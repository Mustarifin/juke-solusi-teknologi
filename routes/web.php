<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\DropdownController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EmployeeController::class, 'index']);

Route::name('employee.')->group(function(){
    Route::get('/employee', [EmployeeController::class, 'index'])->name('index');
    Route::post('/employee/list', [EmployeeController::class, 'listEmployee'])->name('list');
    Route::post('/employee/store', [EmployeeController::class, 'store'])->name('store');
    Route::get('/employee/{employee}/edit', [EmployeeController::class, 'edit'])->name('edit');
    Route::post('/employee/{employee}/edit', [EmployeeController::class, 'update'])->name('update');
    Route::delete('/employee/{employee}', [EmployeeController::class, 'delete'])->name('delete');
});

Route::prefix('dropdown')->group(function(){
    Route::post('province', [DropdownController::class, 'province']);
    Route::post('regency', [DropdownController::class, 'regency']);
    Route::post('position', [DropdownController::class, 'positions']);
    Route::post('bank', [DropdownController::class, 'bank']);
});
