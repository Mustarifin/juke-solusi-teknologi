<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Position;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Bank;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->timestamp('date_of_birth')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->unique()->nullable();
            $table->foreignIdFor(Province::class)->nullable();
            $table->foreignIdFor(Regency::class)->nullable();
            $table->text('address')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('ktp')->nullable();
            $table->text('attach_ktp')->nullable();
            $table->foreignIdFor(Position::class)->nullable();
            $table->foreignIdFor(Bank::class)->nullable();
            $table->string('bank_account_number')->nullable();
            $table->timestamps();
            $table->softDeletesTz($column = 'deleted_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
