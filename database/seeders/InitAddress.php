<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Schema;

class InitAddress extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('villages')) {
            DB::table('villages')->delete();
            DB::table('districts')->delete();
            DB::table('regencies')->delete();
            DB::table('provinces')->delete();
            DB::table('countries')->delete();
        }
        DB::unprepared(file_get_contents(__DIR__.'/../sql/indo.sql'));
    }
}
