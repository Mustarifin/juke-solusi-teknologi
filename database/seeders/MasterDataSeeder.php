<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Position;
use DB;

class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = [
            ['name' => 'HR'],
            ['name' => 'Manager'],
            ['name' => 'Supervisor'],
            ['name' => 'Staff'],
            ['name' => 'Security'],
            ['name' => 'Office Boy'],
            ['name' => 'Enginer'],
        ];
        foreach ($positions as $position) {
            Position::create($position);
        }

        DB::unprepared(file_get_contents(__DIR__.'/../sql/bank.sql'));
    }
}
