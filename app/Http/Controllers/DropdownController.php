<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Position;
use App\Models\Bank;
use App\Models\Filter\PositionFilter;

class DropdownController extends Controller
{
    public function province()
    {
        $provinces = Province::pluck('name', 'id');

        return response()->json($provinces);
    }

    public function regency(Request $request)
    {
        $regencies = Regency::where('province_id', $request->provinceId)->pluck('name', 'id');

        return response()->json($regencies);
    }

    public function positions(Request $request)
    {
        $positions = Position::pluck('name', 'id');

        return response()->json($positions);
    }

    public function bank(Request $request)
    {
        $banks = Bank::pluck('name', 'id');

        return response()->json($banks);
    }
}
