<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Position;
use App\Models\Employee;
use App\Models\Filter\EmployeeFilter;
use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $positions = Position::all();

        return view('pages.employee.index', compact('positions'));
    }

    public function listEmployee(Request $request)
    {
        $employees = Employee::filter(new EmployeeFilter($request))
                                   ->paginate($request->per_page ?? 12)->withQueryString();

        return response()->json([
            'employees' => $employees
        ]);
    }

    public function store(StoreRequest $request)
    {
        $employee = Employee::create($request->datas());

        return response()->json([
            'employee' => $employee
        ]);
    }

    public function edit(Employee $employee)
    {
        return response()->json($employee);
    }

    public function update(Employee $employee, UpdateRequest $request)
    {
        $employee = $employee->update($request->datas());

        return response()->json([
            'employee' => $employee
        ]);
    }

    public function delete(Employee $employee)
    {
        $employee->delete();

        return response()->json([
            'status' => 'Ok'
        ]);
    }
}
