<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'date_of_birth' => '',
            'phone' => 'required|numeric|digits_between:1,13',
            'email' => 'nullable|email|max:255|unique:employees,email',
            'province_id' => 'nullable',
            'regency_id' => 'nullable',
            'address' => 'nullable',
            'zip_code'=> 'nullable|max:255',
            'ktp' => 'required|max:255',
            'position_id' => 'nullable',
            'bank_id' => 'nullable',
            'bank_account_number' => 'nullable',
            'attach_ktp' => 'nullable'
        ];
    }

    public function datas()
    {
        if ($this->has('file')) {
            $this->attach_ktp = self::uploadFile($this->file);
        }
        
        return [
            'name' => $this->first_name . ' ' . $this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'date_of_birth' => Carbon::make($this->date_of_birth)->format('Y-m-d'),
            'phone' => $this->phone,
            'email' => $this->email,
            'province_id' => $this->province_id,
            'regency_id' => $this->regency_id,
            'address' => $this->address,
            'zip_code'=> $this->zip_code,
            'ktp' => $this->ktp,
            'position_id' => $this->position_id,
            'bank_id' => $this->bank_id,
            'bank_account_number' => $this->bank_account_number,
            'attach_ktp' => $this->attach_ktp
        ];
    }

    public static function uploadFile($file)
    {
        $fileName  = $file->getClientOriginalName();

        $disk = \Storage::disk('public');

        $disk->putFileAs('ktp', $file, $fileName);

        $url = $disk->url('ktp'.$fileName);

        return 'ktp/'.$fileName;
    }
}
