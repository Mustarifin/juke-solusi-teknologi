<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Filter\Filterable;

class Employee extends Model
{
    use HasFactory, Filterable;

    protected $guarded = [];
}
