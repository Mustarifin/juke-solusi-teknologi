<?php

namespace App\Models\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Database\Eloquent\Builder;

class EmployeeFilter extends Filter
{
    public function firstName($firstName): Builder
    {
        return $this->builder->where('first_name', 'like' ,'%'.$firstName.'%');
    }

    public function lastName($lastName): Builder
    {
        return $this->builder->where('last_name', 'like' ,'%'.$lastName.'%');
    }

    public function positionId($positionId): Builder
    {
        return $this->builder->where('position_id', $positionId);
    }
}