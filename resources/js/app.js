/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

window.Event = new Vue()

/**
 * Component Employee
 */
 Vue.component('employee', require('./components/employee/index.vue').default);
 Vue.component('employee-thumbnail', require('./components/employee/thumbnail.vue').default);
 Vue.component('employee-list', require('./components/employee/list.vue').default);
 Vue.component('employee-form', require('./components/employee/form.vue').default);

 Vue.component('pagination', require('./components/global/pagination.vue').default);
Vue.component('modal-delete', require('./components/global/modal-delete.vue').default);
 Vue.component('datepicker', require('vuejs-datepicker').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
