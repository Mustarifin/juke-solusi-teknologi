<x-app-layout>
    <!-- Page Content -->
    <employee
    list-employee-url="{{ route('employee.list') }}"
    :positions="{{ $positions }}"
    ></employee>
    <!-- /Page Content -->
    @section('style')
        <!-- Select2 CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/select2.min.css') }}">
        
        <!-- Datetimepicker CSS -->
        <link rel="stylesheet" href="{{ asset('assets/orange/css/bootstrap-datetimepicker.min.css') }}">
    @endsection

    @section('scripts')
        <!-- Select2 JS -->
        <script src="{{ asset('assets/orange/js/select2.min.js') }}"></script>
        
        <!-- Datetimepicker JS -->
        <script src="{{ asset('assets/orange/js/moment.min.js') }}"></script>
        <script src="{{ asset('assets/orange/js/bootstrap-datetimepicker.min.js') }}"></script>
        
    @endsection
</x-app-layout>